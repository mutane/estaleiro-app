import 'package:EstaleiroApp/Model/Database.dart';
import 'package:flutter/material.dart';

class AppRoutes {
  static const Home = "/";
  static const CAIXA = '/view-caixa-caixa';
  static const PRODUTO = '/View-produto-produto';
  static const RELATORIOS = '/View-settings-relatorioGeral';
  static const FATURA = '/View-pdv-fatura';
  static const RELATORIO = '/View-pdv-relatorio';
}
