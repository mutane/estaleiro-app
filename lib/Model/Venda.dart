part of 'Database.dart';

@DataClassName("Venda")
class Vendas extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get produto => integer()
      .nullable()
      .customConstraint('NULL REFERENCES medida_has_produto(id)')();
  IntColumn get quantidade => integer().withDefault(const Constant(1))();
  IntColumn get ordem =>
      integer().nullable().customConstraint('NULL REFERENCES ordens(id)')();
  IntColumn get status =>
      integer().nullable().customConstraint('NULL REFERENCES estados(id)')();
  BoolColumn get foiDescontado =>
      boolean().withDefault(const Constant(false))();

  IntColumn get pagamentoId => integer()
      .customConstraint('NULL REFERENCES pagamentos(id)')
      .withDefault(const Constant(1))();
  RealColumn get vendidoA => real()();
  TextColumn get nomeDoCliente => text()();
  IntColumn get contactoDecliente => integer()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
