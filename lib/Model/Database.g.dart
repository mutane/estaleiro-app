// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Categoria extends DataClass implements Insertable<Categoria> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Categoria(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Categoria.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Categoria(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  CategoriasCompanion toCompanion(bool nullToAbsent) {
    return CategoriasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Categoria.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Categoria(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Categoria copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Categoria(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Categoria(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Categoria &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class CategoriasCompanion extends UpdateCompanion<Categoria> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const CategoriasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  CategoriasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Categoria> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  CategoriasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return CategoriasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoriasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $CategoriasTable extends Categorias
    with TableInfo<$CategoriasTable, Categoria> {
  final GeneratedDatabase _db;
  final String _alias;
  $CategoriasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $CategoriasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'categorias';
  @override
  final String actualTableName = 'categorias';
  @override
  VerificationContext validateIntegrity(Insertable<Categoria> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Categoria map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Categoria.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $CategoriasTable createAlias(String alias) {
    return $CategoriasTable(_db, alias);
  }
}

class Estado extends DataClass implements Insertable<Estado> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Estado(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Estado.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Estado(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  EstadosCompanion toCompanion(bool nullToAbsent) {
    return EstadosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Estado.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Estado(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Estado copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Estado(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Estado(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Estado &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class EstadosCompanion extends UpdateCompanion<Estado> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const EstadosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  EstadosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Estado> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  EstadosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return EstadosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('EstadosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $EstadosTable extends Estados with TableInfo<$EstadosTable, Estado> {
  final GeneratedDatabase _db;
  final String _alias;
  $EstadosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $EstadosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'estados';
  @override
  final String actualTableName = 'estados';
  @override
  VerificationContext validateIntegrity(Insertable<Estado> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Estado map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Estado.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $EstadosTable createAlias(String alias) {
    return $EstadosTable(_db, alias);
  }
}

class Medida extends DataClass implements Insertable<Medida> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Medida(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Medida.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Medida(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  MedidasCompanion toCompanion(bool nullToAbsent) {
    return MedidasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Medida.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Medida(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Medida copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Medida(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Medida(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Medida &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class MedidasCompanion extends UpdateCompanion<Medida> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const MedidasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  MedidasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Medida> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  MedidasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return MedidasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MedidasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $MedidasTable extends Medidas with TableInfo<$MedidasTable, Medida> {
  final GeneratedDatabase _db;
  final String _alias;
  $MedidasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $MedidasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'medidas';
  @override
  final String actualTableName = 'medidas';
  @override
  VerificationContext validateIntegrity(Insertable<Medida> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Medida map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Medida.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $MedidasTable createAlias(String alias) {
    return $MedidasTable(_db, alias);
  }
}

class MedidaHasProduto extends DataClass
    implements Insertable<MedidaHasProduto> {
  final int id;
  final double preco;
  final int produtoId;
  final int medidaId;
  final double desconto;
  final int numeroDeProdutosPorMedida;
  final DateTime createdAt;
  final DateTime updatedAt;
  MedidaHasProduto(
      {@required this.id,
      @required this.preco,
      this.produtoId,
      this.medidaId,
      @required this.desconto,
      @required this.numeroDeProdutosPorMedida,
      @required this.createdAt,
      @required this.updatedAt});
  factory MedidaHasProduto.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return MedidaHasProduto(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      preco:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}preco']),
      produtoId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}produto_id']),
      medidaId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}medida_id']),
      desconto: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}desconto']),
      numeroDeProdutosPorMedida: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}numero_de_produtos_por_medida']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || preco != null) {
      map['preco'] = Variable<double>(preco);
    }
    if (!nullToAbsent || produtoId != null) {
      map['produto_id'] = Variable<int>(produtoId);
    }
    if (!nullToAbsent || medidaId != null) {
      map['medida_id'] = Variable<int>(medidaId);
    }
    if (!nullToAbsent || desconto != null) {
      map['desconto'] = Variable<double>(desconto);
    }
    if (!nullToAbsent || numeroDeProdutosPorMedida != null) {
      map['numero_de_produtos_por_medida'] =
          Variable<int>(numeroDeProdutosPorMedida);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  MedidaHasProdutosCompanion toCompanion(bool nullToAbsent) {
    return MedidaHasProdutosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      preco:
          preco == null && nullToAbsent ? const Value.absent() : Value(preco),
      produtoId: produtoId == null && nullToAbsent
          ? const Value.absent()
          : Value(produtoId),
      medidaId: medidaId == null && nullToAbsent
          ? const Value.absent()
          : Value(medidaId),
      desconto: desconto == null && nullToAbsent
          ? const Value.absent()
          : Value(desconto),
      numeroDeProdutosPorMedida:
          numeroDeProdutosPorMedida == null && nullToAbsent
              ? const Value.absent()
              : Value(numeroDeProdutosPorMedida),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory MedidaHasProduto.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return MedidaHasProduto(
      id: serializer.fromJson<int>(json['id']),
      preco: serializer.fromJson<double>(json['preco']),
      produtoId: serializer.fromJson<int>(json['produtoId']),
      medidaId: serializer.fromJson<int>(json['medidaId']),
      desconto: serializer.fromJson<double>(json['desconto']),
      numeroDeProdutosPorMedida:
          serializer.fromJson<int>(json['numeroDeProdutosPorMedida']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'preco': serializer.toJson<double>(preco),
      'produtoId': serializer.toJson<int>(produtoId),
      'medidaId': serializer.toJson<int>(medidaId),
      'desconto': serializer.toJson<double>(desconto),
      'numeroDeProdutosPorMedida':
          serializer.toJson<int>(numeroDeProdutosPorMedida),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  MedidaHasProduto copyWith(
          {int id,
          double preco,
          int produtoId,
          int medidaId,
          double desconto,
          int numeroDeProdutosPorMedida,
          DateTime createdAt,
          DateTime updatedAt}) =>
      MedidaHasProduto(
        id: id ?? this.id,
        preco: preco ?? this.preco,
        produtoId: produtoId ?? this.produtoId,
        medidaId: medidaId ?? this.medidaId,
        desconto: desconto ?? this.desconto,
        numeroDeProdutosPorMedida:
            numeroDeProdutosPorMedida ?? this.numeroDeProdutosPorMedida,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('MedidaHasProduto(')
          ..write('id: $id, ')
          ..write('preco: $preco, ')
          ..write('produtoId: $produtoId, ')
          ..write('medidaId: $medidaId, ')
          ..write('desconto: $desconto, ')
          ..write('numeroDeProdutosPorMedida: $numeroDeProdutosPorMedida, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          preco.hashCode,
          $mrjc(
              produtoId.hashCode,
              $mrjc(
                  medidaId.hashCode,
                  $mrjc(
                      desconto.hashCode,
                      $mrjc(numeroDeProdutosPorMedida.hashCode,
                          $mrjc(createdAt.hashCode, updatedAt.hashCode))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is MedidaHasProduto &&
          other.id == this.id &&
          other.preco == this.preco &&
          other.produtoId == this.produtoId &&
          other.medidaId == this.medidaId &&
          other.desconto == this.desconto &&
          other.numeroDeProdutosPorMedida == this.numeroDeProdutosPorMedida &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class MedidaHasProdutosCompanion extends UpdateCompanion<MedidaHasProduto> {
  final Value<int> id;
  final Value<double> preco;
  final Value<int> produtoId;
  final Value<int> medidaId;
  final Value<double> desconto;
  final Value<int> numeroDeProdutosPorMedida;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const MedidaHasProdutosCompanion({
    this.id = const Value.absent(),
    this.preco = const Value.absent(),
    this.produtoId = const Value.absent(),
    this.medidaId = const Value.absent(),
    this.desconto = const Value.absent(),
    this.numeroDeProdutosPorMedida = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  MedidaHasProdutosCompanion.insert({
    this.id = const Value.absent(),
    @required double preco,
    this.produtoId = const Value.absent(),
    this.medidaId = const Value.absent(),
    @required double desconto,
    this.numeroDeProdutosPorMedida = const Value.absent(),
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : preco = Value(preco),
        desconto = Value(desconto),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<MedidaHasProduto> custom({
    Expression<int> id,
    Expression<double> preco,
    Expression<int> produtoId,
    Expression<int> medidaId,
    Expression<double> desconto,
    Expression<int> numeroDeProdutosPorMedida,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (preco != null) 'preco': preco,
      if (produtoId != null) 'produto_id': produtoId,
      if (medidaId != null) 'medida_id': medidaId,
      if (desconto != null) 'desconto': desconto,
      if (numeroDeProdutosPorMedida != null)
        'numero_de_produtos_por_medida': numeroDeProdutosPorMedida,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  MedidaHasProdutosCompanion copyWith(
      {Value<int> id,
      Value<double> preco,
      Value<int> produtoId,
      Value<int> medidaId,
      Value<double> desconto,
      Value<int> numeroDeProdutosPorMedida,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return MedidaHasProdutosCompanion(
      id: id ?? this.id,
      preco: preco ?? this.preco,
      produtoId: produtoId ?? this.produtoId,
      medidaId: medidaId ?? this.medidaId,
      desconto: desconto ?? this.desconto,
      numeroDeProdutosPorMedida:
          numeroDeProdutosPorMedida ?? this.numeroDeProdutosPorMedida,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (preco.present) {
      map['preco'] = Variable<double>(preco.value);
    }
    if (produtoId.present) {
      map['produto_id'] = Variable<int>(produtoId.value);
    }
    if (medidaId.present) {
      map['medida_id'] = Variable<int>(medidaId.value);
    }
    if (desconto.present) {
      map['desconto'] = Variable<double>(desconto.value);
    }
    if (numeroDeProdutosPorMedida.present) {
      map['numero_de_produtos_por_medida'] =
          Variable<int>(numeroDeProdutosPorMedida.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MedidaHasProdutosCompanion(')
          ..write('id: $id, ')
          ..write('preco: $preco, ')
          ..write('produtoId: $produtoId, ')
          ..write('medidaId: $medidaId, ')
          ..write('desconto: $desconto, ')
          ..write('numeroDeProdutosPorMedida: $numeroDeProdutosPorMedida, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $MedidaHasProdutosTable extends MedidaHasProdutos
    with TableInfo<$MedidaHasProdutosTable, MedidaHasProduto> {
  final GeneratedDatabase _db;
  final String _alias;
  $MedidaHasProdutosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _precoMeta = const VerificationMeta('preco');
  GeneratedRealColumn _preco;
  @override
  GeneratedRealColumn get preco => _preco ??= _constructPreco();
  GeneratedRealColumn _constructPreco() {
    return GeneratedRealColumn(
      'preco',
      $tableName,
      false,
    );
  }

  final VerificationMeta _produtoIdMeta = const VerificationMeta('produtoId');
  GeneratedIntColumn _produtoId;
  @override
  GeneratedIntColumn get produtoId => _produtoId ??= _constructProdutoId();
  GeneratedIntColumn _constructProdutoId() {
    return GeneratedIntColumn('produto_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES produtos(id)');
  }

  final VerificationMeta _medidaIdMeta = const VerificationMeta('medidaId');
  GeneratedIntColumn _medidaId;
  @override
  GeneratedIntColumn get medidaId => _medidaId ??= _constructMedidaId();
  GeneratedIntColumn _constructMedidaId() {
    return GeneratedIntColumn('medida_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES medidas(id)');
  }

  final VerificationMeta _descontoMeta = const VerificationMeta('desconto');
  GeneratedRealColumn _desconto;
  @override
  GeneratedRealColumn get desconto => _desconto ??= _constructDesconto();
  GeneratedRealColumn _constructDesconto() {
    return GeneratedRealColumn(
      'desconto',
      $tableName,
      false,
    );
  }

  final VerificationMeta _numeroDeProdutosPorMedidaMeta =
      const VerificationMeta('numeroDeProdutosPorMedida');
  GeneratedIntColumn _numeroDeProdutosPorMedida;
  @override
  GeneratedIntColumn get numeroDeProdutosPorMedida =>
      _numeroDeProdutosPorMedida ??= _constructNumeroDeProdutosPorMedida();
  GeneratedIntColumn _constructNumeroDeProdutosPorMedida() {
    return GeneratedIntColumn(
        'numero_de_produtos_por_medida', $tableName, false,
        defaultValue: const Constant(1));
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        preco,
        produtoId,
        medidaId,
        desconto,
        numeroDeProdutosPorMedida,
        createdAt,
        updatedAt
      ];
  @override
  $MedidaHasProdutosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'medida_has_produtos';
  @override
  final String actualTableName = 'medida_has_produtos';
  @override
  VerificationContext validateIntegrity(Insertable<MedidaHasProduto> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('preco')) {
      context.handle(
          _precoMeta, preco.isAcceptableOrUnknown(data['preco'], _precoMeta));
    } else if (isInserting) {
      context.missing(_precoMeta);
    }
    if (data.containsKey('produto_id')) {
      context.handle(_produtoIdMeta,
          produtoId.isAcceptableOrUnknown(data['produto_id'], _produtoIdMeta));
    }
    if (data.containsKey('medida_id')) {
      context.handle(_medidaIdMeta,
          medidaId.isAcceptableOrUnknown(data['medida_id'], _medidaIdMeta));
    }
    if (data.containsKey('desconto')) {
      context.handle(_descontoMeta,
          desconto.isAcceptableOrUnknown(data['desconto'], _descontoMeta));
    } else if (isInserting) {
      context.missing(_descontoMeta);
    }
    if (data.containsKey('numero_de_produtos_por_medida')) {
      context.handle(
          _numeroDeProdutosPorMedidaMeta,
          numeroDeProdutosPorMedida.isAcceptableOrUnknown(
              data['numero_de_produtos_por_medida'],
              _numeroDeProdutosPorMedidaMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  MedidaHasProduto map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return MedidaHasProduto.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $MedidaHasProdutosTable createAlias(String alias) {
    return $MedidaHasProdutosTable(_db, alias);
  }
}

class Ordem extends DataClass implements Insertable<Ordem> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Ordem(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Ordem.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Ordem(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  OrdensCompanion toCompanion(bool nullToAbsent) {
    return OrdensCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Ordem.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Ordem(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Ordem copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Ordem(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Ordem(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Ordem &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class OrdensCompanion extends UpdateCompanion<Ordem> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const OrdensCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  OrdensCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Ordem> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  OrdensCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return OrdensCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrdensCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $OrdensTable extends Ordens with TableInfo<$OrdensTable, Ordem> {
  final GeneratedDatabase _db;
  final String _alias;
  $OrdensTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $OrdensTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'ordens';
  @override
  final String actualTableName = 'ordens';
  @override
  VerificationContext validateIntegrity(Insertable<Ordem> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Ordem map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Ordem.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $OrdensTable createAlias(String alias) {
    return $OrdensTable(_db, alias);
  }
}

class Pagamento extends DataClass implements Insertable<Pagamento> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Pagamento(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Pagamento.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Pagamento(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  PagamentosCompanion toCompanion(bool nullToAbsent) {
    return PagamentosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Pagamento.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Pagamento(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Pagamento copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Pagamento(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Pagamento(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Pagamento &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class PagamentosCompanion extends UpdateCompanion<Pagamento> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const PagamentosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  PagamentosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Pagamento> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  PagamentosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return PagamentosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PagamentosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $PagamentosTable extends Pagamentos
    with TableInfo<$PagamentosTable, Pagamento> {
  final GeneratedDatabase _db;
  final String _alias;
  $PagamentosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $PagamentosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'pagamentos';
  @override
  final String actualTableName = 'pagamentos';
  @override
  VerificationContext validateIntegrity(Insertable<Pagamento> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Pagamento map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Pagamento.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $PagamentosTable createAlias(String alias) {
    return $PagamentosTable(_db, alias);
  }
}

class Produto extends DataClass implements Insertable<Produto> {
  final int id;
  final String nome;
  final String descricao;
  final String codSistema;
  final int categoriaId;
  final int quantidade;
  final DateTime createdAt;
  final DateTime updatedAt;
  Produto(
      {@required this.id,
      @required this.nome,
      @required this.descricao,
      @required this.codSistema,
      this.categoriaId,
      @required this.quantidade,
      @required this.createdAt,
      @required this.updatedAt});
  factory Produto.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Produto(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      descricao: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}descricao']),
      codSistema: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}cod_sistema']),
      categoriaId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}categoria_id']),
      quantidade:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}quantidade']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || descricao != null) {
      map['descricao'] = Variable<String>(descricao);
    }
    if (!nullToAbsent || codSistema != null) {
      map['cod_sistema'] = Variable<String>(codSistema);
    }
    if (!nullToAbsent || categoriaId != null) {
      map['categoria_id'] = Variable<int>(categoriaId);
    }
    if (!nullToAbsent || quantidade != null) {
      map['quantidade'] = Variable<int>(quantidade);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ProdutosCompanion toCompanion(bool nullToAbsent) {
    return ProdutosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      descricao: descricao == null && nullToAbsent
          ? const Value.absent()
          : Value(descricao),
      codSistema: codSistema == null && nullToAbsent
          ? const Value.absent()
          : Value(codSistema),
      categoriaId: categoriaId == null && nullToAbsent
          ? const Value.absent()
          : Value(categoriaId),
      quantidade: quantidade == null && nullToAbsent
          ? const Value.absent()
          : Value(quantidade),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Produto.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Produto(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      descricao: serializer.fromJson<String>(json['descricao']),
      codSistema: serializer.fromJson<String>(json['codSistema']),
      categoriaId: serializer.fromJson<int>(json['categoriaId']),
      quantidade: serializer.fromJson<int>(json['quantidade']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'descricao': serializer.toJson<String>(descricao),
      'codSistema': serializer.toJson<String>(codSistema),
      'categoriaId': serializer.toJson<int>(categoriaId),
      'quantidade': serializer.toJson<int>(quantidade),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Produto copyWith(
          {int id,
          String nome,
          String descricao,
          String codSistema,
          int categoriaId,
          int quantidade,
          DateTime createdAt,
          DateTime updatedAt}) =>
      Produto(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        descricao: descricao ?? this.descricao,
        codSistema: codSistema ?? this.codSistema,
        categoriaId: categoriaId ?? this.categoriaId,
        quantidade: quantidade ?? this.quantidade,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Produto(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('descricao: $descricao, ')
          ..write('codSistema: $codSistema, ')
          ..write('categoriaId: $categoriaId, ')
          ..write('quantidade: $quantidade, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          nome.hashCode,
          $mrjc(
              descricao.hashCode,
              $mrjc(
                  codSistema.hashCode,
                  $mrjc(
                      categoriaId.hashCode,
                      $mrjc(quantidade.hashCode,
                          $mrjc(createdAt.hashCode, updatedAt.hashCode))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Produto &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.descricao == this.descricao &&
          other.codSistema == this.codSistema &&
          other.categoriaId == this.categoriaId &&
          other.quantidade == this.quantidade &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ProdutosCompanion extends UpdateCompanion<Produto> {
  final Value<int> id;
  final Value<String> nome;
  final Value<String> descricao;
  final Value<String> codSistema;
  final Value<int> categoriaId;
  final Value<int> quantidade;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ProdutosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.descricao = const Value.absent(),
    this.codSistema = const Value.absent(),
    this.categoriaId = const Value.absent(),
    this.quantidade = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ProdutosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required String descricao,
    @required String codSistema,
    this.categoriaId = const Value.absent(),
    @required int quantidade,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        descricao = Value(descricao),
        codSistema = Value(codSistema),
        quantidade = Value(quantidade),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Produto> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<String> descricao,
    Expression<String> codSistema,
    Expression<int> categoriaId,
    Expression<int> quantidade,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (descricao != null) 'descricao': descricao,
      if (codSistema != null) 'cod_sistema': codSistema,
      if (categoriaId != null) 'categoria_id': categoriaId,
      if (quantidade != null) 'quantidade': quantidade,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ProdutosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<String> descricao,
      Value<String> codSistema,
      Value<int> categoriaId,
      Value<int> quantidade,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ProdutosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      descricao: descricao ?? this.descricao,
      codSistema: codSistema ?? this.codSistema,
      categoriaId: categoriaId ?? this.categoriaId,
      quantidade: quantidade ?? this.quantidade,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (descricao.present) {
      map['descricao'] = Variable<String>(descricao.value);
    }
    if (codSistema.present) {
      map['cod_sistema'] = Variable<String>(codSistema.value);
    }
    if (categoriaId.present) {
      map['categoria_id'] = Variable<int>(categoriaId.value);
    }
    if (quantidade.present) {
      map['quantidade'] = Variable<int>(quantidade.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProdutosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('descricao: $descricao, ')
          ..write('codSistema: $codSistema, ')
          ..write('categoriaId: $categoriaId, ')
          ..write('quantidade: $quantidade, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ProdutosTable extends Produtos with TableInfo<$ProdutosTable, Produto> {
  final GeneratedDatabase _db;
  final String _alias;
  $ProdutosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _descricaoMeta = const VerificationMeta('descricao');
  GeneratedTextColumn _descricao;
  @override
  GeneratedTextColumn get descricao => _descricao ??= _constructDescricao();
  GeneratedTextColumn _constructDescricao() {
    return GeneratedTextColumn('descricao', $tableName, false,
        maxTextLength: 255);
  }

  final VerificationMeta _codSistemaMeta = const VerificationMeta('codSistema');
  GeneratedTextColumn _codSistema;
  @override
  GeneratedTextColumn get codSistema => _codSistema ??= _constructCodSistema();
  GeneratedTextColumn _constructCodSistema() {
    return GeneratedTextColumn('cod_sistema', $tableName, false,
        maxTextLength: 50);
  }

  final VerificationMeta _categoriaIdMeta =
      const VerificationMeta('categoriaId');
  GeneratedIntColumn _categoriaId;
  @override
  GeneratedIntColumn get categoriaId =>
      _categoriaId ??= _constructCategoriaId();
  GeneratedIntColumn _constructCategoriaId() {
    return GeneratedIntColumn('categoria_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES sub_categorias(id)');
  }

  final VerificationMeta _quantidadeMeta = const VerificationMeta('quantidade');
  GeneratedIntColumn _quantidade;
  @override
  GeneratedIntColumn get quantidade => _quantidade ??= _constructQuantidade();
  GeneratedIntColumn _constructQuantidade() {
    return GeneratedIntColumn(
      'quantidade',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        nome,
        descricao,
        codSistema,
        categoriaId,
        quantidade,
        createdAt,
        updatedAt
      ];
  @override
  $ProdutosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'produtos';
  @override
  final String actualTableName = 'produtos';
  @override
  VerificationContext validateIntegrity(Insertable<Produto> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('descricao')) {
      context.handle(_descricaoMeta,
          descricao.isAcceptableOrUnknown(data['descricao'], _descricaoMeta));
    } else if (isInserting) {
      context.missing(_descricaoMeta);
    }
    if (data.containsKey('cod_sistema')) {
      context.handle(
          _codSistemaMeta,
          codSistema.isAcceptableOrUnknown(
              data['cod_sistema'], _codSistemaMeta));
    } else if (isInserting) {
      context.missing(_codSistemaMeta);
    }
    if (data.containsKey('categoria_id')) {
      context.handle(
          _categoriaIdMeta,
          categoriaId.isAcceptableOrUnknown(
              data['categoria_id'], _categoriaIdMeta));
    }
    if (data.containsKey('quantidade')) {
      context.handle(
          _quantidadeMeta,
          quantidade.isAcceptableOrUnknown(
              data['quantidade'], _quantidadeMeta));
    } else if (isInserting) {
      context.missing(_quantidadeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Produto map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Produto.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ProdutosTable createAlias(String alias) {
    return $ProdutosTable(_db, alias);
  }
}

class SubCategoria extends DataClass implements Insertable<SubCategoria> {
  final int id;
  final String nome;
  final int categoriaId;
  final DateTime createdAt;
  final DateTime updatedAt;
  SubCategoria(
      {@required this.id,
      @required this.nome,
      this.categoriaId,
      @required this.createdAt,
      @required this.updatedAt});
  factory SubCategoria.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return SubCategoria(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      categoriaId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}categoria_id']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || categoriaId != null) {
      map['categoria_id'] = Variable<int>(categoriaId);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  SubCategoriasCompanion toCompanion(bool nullToAbsent) {
    return SubCategoriasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      categoriaId: categoriaId == null && nullToAbsent
          ? const Value.absent()
          : Value(categoriaId),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory SubCategoria.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return SubCategoria(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      categoriaId: serializer.fromJson<int>(json['categoriaId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'categoriaId': serializer.toJson<int>(categoriaId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  SubCategoria copyWith(
          {int id,
          String nome,
          int categoriaId,
          DateTime createdAt,
          DateTime updatedAt}) =>
      SubCategoria(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        categoriaId: categoriaId ?? this.categoriaId,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('SubCategoria(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('categoriaId: $categoriaId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          nome.hashCode,
          $mrjc(categoriaId.hashCode,
              $mrjc(createdAt.hashCode, updatedAt.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is SubCategoria &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.categoriaId == this.categoriaId &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class SubCategoriasCompanion extends UpdateCompanion<SubCategoria> {
  final Value<int> id;
  final Value<String> nome;
  final Value<int> categoriaId;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const SubCategoriasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.categoriaId = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  SubCategoriasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    this.categoriaId = const Value.absent(),
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<SubCategoria> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<int> categoriaId,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (categoriaId != null) 'categoria_id': categoriaId,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  SubCategoriasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<int> categoriaId,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return SubCategoriasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      categoriaId: categoriaId ?? this.categoriaId,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (categoriaId.present) {
      map['categoria_id'] = Variable<int>(categoriaId.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SubCategoriasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('categoriaId: $categoriaId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $SubCategoriasTable extends SubCategorias
    with TableInfo<$SubCategoriasTable, SubCategoria> {
  final GeneratedDatabase _db;
  final String _alias;
  $SubCategoriasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _categoriaIdMeta =
      const VerificationMeta('categoriaId');
  GeneratedIntColumn _categoriaId;
  @override
  GeneratedIntColumn get categoriaId =>
      _categoriaId ??= _constructCategoriaId();
  GeneratedIntColumn _constructCategoriaId() {
    return GeneratedIntColumn('categoria_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES categorias(id)');
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, nome, categoriaId, createdAt, updatedAt];
  @override
  $SubCategoriasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'sub_categorias';
  @override
  final String actualTableName = 'sub_categorias';
  @override
  VerificationContext validateIntegrity(Insertable<SubCategoria> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('categoria_id')) {
      context.handle(
          _categoriaIdMeta,
          categoriaId.isAcceptableOrUnknown(
              data['categoria_id'], _categoriaIdMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SubCategoria map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return SubCategoria.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $SubCategoriasTable createAlias(String alias) {
    return $SubCategoriasTable(_db, alias);
  }
}

class Venda extends DataClass implements Insertable<Venda> {
  final int id;
  final int produto;
  final int quantidade;
  final int ordem;
  final int status;
  final bool foiDescontado;
  final int pagamentoId;
  final double vendidoA;
  final String nomeDoCliente;
  final int contactoDecliente;
  final DateTime createdAt;
  final DateTime updatedAt;
  Venda(
      {@required this.id,
      this.produto,
      @required this.quantidade,
      this.ordem,
      this.status,
      @required this.foiDescontado,
      @required this.pagamentoId,
      @required this.vendidoA,
      @required this.nomeDoCliente,
      @required this.contactoDecliente,
      @required this.createdAt,
      @required this.updatedAt});
  factory Venda.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final boolType = db.typeSystem.forDartType<bool>();
    final doubleType = db.typeSystem.forDartType<double>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Venda(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      produto:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}produto']),
      quantidade:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}quantidade']),
      ordem: intType.mapFromDatabaseResponse(data['${effectivePrefix}ordem']),
      status: intType.mapFromDatabaseResponse(data['${effectivePrefix}status']),
      foiDescontado: boolType
          .mapFromDatabaseResponse(data['${effectivePrefix}foi_descontado']),
      pagamentoId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}pagamento_id']),
      vendidoA: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}vendido_a']),
      nomeDoCliente: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}nome_do_cliente']),
      contactoDecliente: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}contacto_decliente']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || produto != null) {
      map['produto'] = Variable<int>(produto);
    }
    if (!nullToAbsent || quantidade != null) {
      map['quantidade'] = Variable<int>(quantidade);
    }
    if (!nullToAbsent || ordem != null) {
      map['ordem'] = Variable<int>(ordem);
    }
    if (!nullToAbsent || status != null) {
      map['status'] = Variable<int>(status);
    }
    if (!nullToAbsent || foiDescontado != null) {
      map['foi_descontado'] = Variable<bool>(foiDescontado);
    }
    if (!nullToAbsent || pagamentoId != null) {
      map['pagamento_id'] = Variable<int>(pagamentoId);
    }
    if (!nullToAbsent || vendidoA != null) {
      map['vendido_a'] = Variable<double>(vendidoA);
    }
    if (!nullToAbsent || nomeDoCliente != null) {
      map['nome_do_cliente'] = Variable<String>(nomeDoCliente);
    }
    if (!nullToAbsent || contactoDecliente != null) {
      map['contacto_decliente'] = Variable<int>(contactoDecliente);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  VendasCompanion toCompanion(bool nullToAbsent) {
    return VendasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      produto: produto == null && nullToAbsent
          ? const Value.absent()
          : Value(produto),
      quantidade: quantidade == null && nullToAbsent
          ? const Value.absent()
          : Value(quantidade),
      ordem:
          ordem == null && nullToAbsent ? const Value.absent() : Value(ordem),
      status:
          status == null && nullToAbsent ? const Value.absent() : Value(status),
      foiDescontado: foiDescontado == null && nullToAbsent
          ? const Value.absent()
          : Value(foiDescontado),
      pagamentoId: pagamentoId == null && nullToAbsent
          ? const Value.absent()
          : Value(pagamentoId),
      vendidoA: vendidoA == null && nullToAbsent
          ? const Value.absent()
          : Value(vendidoA),
      nomeDoCliente: nomeDoCliente == null && nullToAbsent
          ? const Value.absent()
          : Value(nomeDoCliente),
      contactoDecliente: contactoDecliente == null && nullToAbsent
          ? const Value.absent()
          : Value(contactoDecliente),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Venda.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Venda(
      id: serializer.fromJson<int>(json['id']),
      produto: serializer.fromJson<int>(json['produto']),
      quantidade: serializer.fromJson<int>(json['quantidade']),
      ordem: serializer.fromJson<int>(json['ordem']),
      status: serializer.fromJson<int>(json['status']),
      foiDescontado: serializer.fromJson<bool>(json['foiDescontado']),
      pagamentoId: serializer.fromJson<int>(json['pagamentoId']),
      vendidoA: serializer.fromJson<double>(json['vendidoA']),
      nomeDoCliente: serializer.fromJson<String>(json['nomeDoCliente']),
      contactoDecliente: serializer.fromJson<int>(json['contactoDecliente']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'produto': serializer.toJson<int>(produto),
      'quantidade': serializer.toJson<int>(quantidade),
      'ordem': serializer.toJson<int>(ordem),
      'status': serializer.toJson<int>(status),
      'foiDescontado': serializer.toJson<bool>(foiDescontado),
      'pagamentoId': serializer.toJson<int>(pagamentoId),
      'vendidoA': serializer.toJson<double>(vendidoA),
      'nomeDoCliente': serializer.toJson<String>(nomeDoCliente),
      'contactoDecliente': serializer.toJson<int>(contactoDecliente),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Venda copyWith(
          {int id,
          int produto,
          int quantidade,
          int ordem,
          int status,
          bool foiDescontado,
          int pagamentoId,
          double vendidoA,
          String nomeDoCliente,
          int contactoDecliente,
          DateTime createdAt,
          DateTime updatedAt}) =>
      Venda(
        id: id ?? this.id,
        produto: produto ?? this.produto,
        quantidade: quantidade ?? this.quantidade,
        ordem: ordem ?? this.ordem,
        status: status ?? this.status,
        foiDescontado: foiDescontado ?? this.foiDescontado,
        pagamentoId: pagamentoId ?? this.pagamentoId,
        vendidoA: vendidoA ?? this.vendidoA,
        nomeDoCliente: nomeDoCliente ?? this.nomeDoCliente,
        contactoDecliente: contactoDecliente ?? this.contactoDecliente,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Venda(')
          ..write('id: $id, ')
          ..write('produto: $produto, ')
          ..write('quantidade: $quantidade, ')
          ..write('ordem: $ordem, ')
          ..write('status: $status, ')
          ..write('foiDescontado: $foiDescontado, ')
          ..write('pagamentoId: $pagamentoId, ')
          ..write('vendidoA: $vendidoA, ')
          ..write('nomeDoCliente: $nomeDoCliente, ')
          ..write('contactoDecliente: $contactoDecliente, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          produto.hashCode,
          $mrjc(
              quantidade.hashCode,
              $mrjc(
                  ordem.hashCode,
                  $mrjc(
                      status.hashCode,
                      $mrjc(
                          foiDescontado.hashCode,
                          $mrjc(
                              pagamentoId.hashCode,
                              $mrjc(
                                  vendidoA.hashCode,
                                  $mrjc(
                                      nomeDoCliente.hashCode,
                                      $mrjc(
                                          contactoDecliente.hashCode,
                                          $mrjc(createdAt.hashCode,
                                              updatedAt.hashCode))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Venda &&
          other.id == this.id &&
          other.produto == this.produto &&
          other.quantidade == this.quantidade &&
          other.ordem == this.ordem &&
          other.status == this.status &&
          other.foiDescontado == this.foiDescontado &&
          other.pagamentoId == this.pagamentoId &&
          other.vendidoA == this.vendidoA &&
          other.nomeDoCliente == this.nomeDoCliente &&
          other.contactoDecliente == this.contactoDecliente &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class VendasCompanion extends UpdateCompanion<Venda> {
  final Value<int> id;
  final Value<int> produto;
  final Value<int> quantidade;
  final Value<int> ordem;
  final Value<int> status;
  final Value<bool> foiDescontado;
  final Value<int> pagamentoId;
  final Value<double> vendidoA;
  final Value<String> nomeDoCliente;
  final Value<int> contactoDecliente;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const VendasCompanion({
    this.id = const Value.absent(),
    this.produto = const Value.absent(),
    this.quantidade = const Value.absent(),
    this.ordem = const Value.absent(),
    this.status = const Value.absent(),
    this.foiDescontado = const Value.absent(),
    this.pagamentoId = const Value.absent(),
    this.vendidoA = const Value.absent(),
    this.nomeDoCliente = const Value.absent(),
    this.contactoDecliente = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  VendasCompanion.insert({
    this.id = const Value.absent(),
    this.produto = const Value.absent(),
    this.quantidade = const Value.absent(),
    this.ordem = const Value.absent(),
    this.status = const Value.absent(),
    this.foiDescontado = const Value.absent(),
    this.pagamentoId = const Value.absent(),
    @required double vendidoA,
    @required String nomeDoCliente,
    @required int contactoDecliente,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : vendidoA = Value(vendidoA),
        nomeDoCliente = Value(nomeDoCliente),
        contactoDecliente = Value(contactoDecliente),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Venda> custom({
    Expression<int> id,
    Expression<int> produto,
    Expression<int> quantidade,
    Expression<int> ordem,
    Expression<int> status,
    Expression<bool> foiDescontado,
    Expression<int> pagamentoId,
    Expression<double> vendidoA,
    Expression<String> nomeDoCliente,
    Expression<int> contactoDecliente,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (produto != null) 'produto': produto,
      if (quantidade != null) 'quantidade': quantidade,
      if (ordem != null) 'ordem': ordem,
      if (status != null) 'status': status,
      if (foiDescontado != null) 'foi_descontado': foiDescontado,
      if (pagamentoId != null) 'pagamento_id': pagamentoId,
      if (vendidoA != null) 'vendido_a': vendidoA,
      if (nomeDoCliente != null) 'nome_do_cliente': nomeDoCliente,
      if (contactoDecliente != null) 'contacto_decliente': contactoDecliente,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  VendasCompanion copyWith(
      {Value<int> id,
      Value<int> produto,
      Value<int> quantidade,
      Value<int> ordem,
      Value<int> status,
      Value<bool> foiDescontado,
      Value<int> pagamentoId,
      Value<double> vendidoA,
      Value<String> nomeDoCliente,
      Value<int> contactoDecliente,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return VendasCompanion(
      id: id ?? this.id,
      produto: produto ?? this.produto,
      quantidade: quantidade ?? this.quantidade,
      ordem: ordem ?? this.ordem,
      status: status ?? this.status,
      foiDescontado: foiDescontado ?? this.foiDescontado,
      pagamentoId: pagamentoId ?? this.pagamentoId,
      vendidoA: vendidoA ?? this.vendidoA,
      nomeDoCliente: nomeDoCliente ?? this.nomeDoCliente,
      contactoDecliente: contactoDecliente ?? this.contactoDecliente,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (produto.present) {
      map['produto'] = Variable<int>(produto.value);
    }
    if (quantidade.present) {
      map['quantidade'] = Variable<int>(quantidade.value);
    }
    if (ordem.present) {
      map['ordem'] = Variable<int>(ordem.value);
    }
    if (status.present) {
      map['status'] = Variable<int>(status.value);
    }
    if (foiDescontado.present) {
      map['foi_descontado'] = Variable<bool>(foiDescontado.value);
    }
    if (pagamentoId.present) {
      map['pagamento_id'] = Variable<int>(pagamentoId.value);
    }
    if (vendidoA.present) {
      map['vendido_a'] = Variable<double>(vendidoA.value);
    }
    if (nomeDoCliente.present) {
      map['nome_do_cliente'] = Variable<String>(nomeDoCliente.value);
    }
    if (contactoDecliente.present) {
      map['contacto_decliente'] = Variable<int>(contactoDecliente.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('VendasCompanion(')
          ..write('id: $id, ')
          ..write('produto: $produto, ')
          ..write('quantidade: $quantidade, ')
          ..write('ordem: $ordem, ')
          ..write('status: $status, ')
          ..write('foiDescontado: $foiDescontado, ')
          ..write('pagamentoId: $pagamentoId, ')
          ..write('vendidoA: $vendidoA, ')
          ..write('nomeDoCliente: $nomeDoCliente, ')
          ..write('contactoDecliente: $contactoDecliente, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $VendasTable extends Vendas with TableInfo<$VendasTable, Venda> {
  final GeneratedDatabase _db;
  final String _alias;
  $VendasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _produtoMeta = const VerificationMeta('produto');
  GeneratedIntColumn _produto;
  @override
  GeneratedIntColumn get produto => _produto ??= _constructProduto();
  GeneratedIntColumn _constructProduto() {
    return GeneratedIntColumn('produto', $tableName, true,
        $customConstraints: 'NULL REFERENCES medida_has_produto(id)');
  }

  final VerificationMeta _quantidadeMeta = const VerificationMeta('quantidade');
  GeneratedIntColumn _quantidade;
  @override
  GeneratedIntColumn get quantidade => _quantidade ??= _constructQuantidade();
  GeneratedIntColumn _constructQuantidade() {
    return GeneratedIntColumn('quantidade', $tableName, false,
        defaultValue: const Constant(1));
  }

  final VerificationMeta _ordemMeta = const VerificationMeta('ordem');
  GeneratedIntColumn _ordem;
  @override
  GeneratedIntColumn get ordem => _ordem ??= _constructOrdem();
  GeneratedIntColumn _constructOrdem() {
    return GeneratedIntColumn('ordem', $tableName, true,
        $customConstraints: 'NULL REFERENCES ordens(id)');
  }

  final VerificationMeta _statusMeta = const VerificationMeta('status');
  GeneratedIntColumn _status;
  @override
  GeneratedIntColumn get status => _status ??= _constructStatus();
  GeneratedIntColumn _constructStatus() {
    return GeneratedIntColumn('status', $tableName, true,
        $customConstraints: 'NULL REFERENCES estados(id)');
  }

  final VerificationMeta _foiDescontadoMeta =
      const VerificationMeta('foiDescontado');
  GeneratedBoolColumn _foiDescontado;
  @override
  GeneratedBoolColumn get foiDescontado =>
      _foiDescontado ??= _constructFoiDescontado();
  GeneratedBoolColumn _constructFoiDescontado() {
    return GeneratedBoolColumn('foi_descontado', $tableName, false,
        defaultValue: const Constant(false));
  }

  final VerificationMeta _pagamentoIdMeta =
      const VerificationMeta('pagamentoId');
  GeneratedIntColumn _pagamentoId;
  @override
  GeneratedIntColumn get pagamentoId =>
      _pagamentoId ??= _constructPagamentoId();
  GeneratedIntColumn _constructPagamentoId() {
    return GeneratedIntColumn('pagamento_id', $tableName, false,
        $customConstraints: 'NULL REFERENCES pagamentos(id)',
        defaultValue: const Constant(1));
  }

  final VerificationMeta _vendidoAMeta = const VerificationMeta('vendidoA');
  GeneratedRealColumn _vendidoA;
  @override
  GeneratedRealColumn get vendidoA => _vendidoA ??= _constructVendidoA();
  GeneratedRealColumn _constructVendidoA() {
    return GeneratedRealColumn(
      'vendido_a',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nomeDoClienteMeta =
      const VerificationMeta('nomeDoCliente');
  GeneratedTextColumn _nomeDoCliente;
  @override
  GeneratedTextColumn get nomeDoCliente =>
      _nomeDoCliente ??= _constructNomeDoCliente();
  GeneratedTextColumn _constructNomeDoCliente() {
    return GeneratedTextColumn(
      'nome_do_cliente',
      $tableName,
      false,
    );
  }

  final VerificationMeta _contactoDeclienteMeta =
      const VerificationMeta('contactoDecliente');
  GeneratedIntColumn _contactoDecliente;
  @override
  GeneratedIntColumn get contactoDecliente =>
      _contactoDecliente ??= _constructContactoDecliente();
  GeneratedIntColumn _constructContactoDecliente() {
    return GeneratedIntColumn(
      'contacto_decliente',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        produto,
        quantidade,
        ordem,
        status,
        foiDescontado,
        pagamentoId,
        vendidoA,
        nomeDoCliente,
        contactoDecliente,
        createdAt,
        updatedAt
      ];
  @override
  $VendasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'vendas';
  @override
  final String actualTableName = 'vendas';
  @override
  VerificationContext validateIntegrity(Insertable<Venda> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('produto')) {
      context.handle(_produtoMeta,
          produto.isAcceptableOrUnknown(data['produto'], _produtoMeta));
    }
    if (data.containsKey('quantidade')) {
      context.handle(
          _quantidadeMeta,
          quantidade.isAcceptableOrUnknown(
              data['quantidade'], _quantidadeMeta));
    }
    if (data.containsKey('ordem')) {
      context.handle(
          _ordemMeta, ordem.isAcceptableOrUnknown(data['ordem'], _ordemMeta));
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status'], _statusMeta));
    }
    if (data.containsKey('foi_descontado')) {
      context.handle(
          _foiDescontadoMeta,
          foiDescontado.isAcceptableOrUnknown(
              data['foi_descontado'], _foiDescontadoMeta));
    }
    if (data.containsKey('pagamento_id')) {
      context.handle(
          _pagamentoIdMeta,
          pagamentoId.isAcceptableOrUnknown(
              data['pagamento_id'], _pagamentoIdMeta));
    }
    if (data.containsKey('vendido_a')) {
      context.handle(_vendidoAMeta,
          vendidoA.isAcceptableOrUnknown(data['vendido_a'], _vendidoAMeta));
    } else if (isInserting) {
      context.missing(_vendidoAMeta);
    }
    if (data.containsKey('nome_do_cliente')) {
      context.handle(
          _nomeDoClienteMeta,
          nomeDoCliente.isAcceptableOrUnknown(
              data['nome_do_cliente'], _nomeDoClienteMeta));
    } else if (isInserting) {
      context.missing(_nomeDoClienteMeta);
    }
    if (data.containsKey('contacto_decliente')) {
      context.handle(
          _contactoDeclienteMeta,
          contactoDecliente.isAcceptableOrUnknown(
              data['contacto_decliente'], _contactoDeclienteMeta));
    } else if (isInserting) {
      context.missing(_contactoDeclienteMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Venda map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Venda.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $VendasTable createAlias(String alias) {
    return $VendasTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $CategoriasTable _categorias;
  $CategoriasTable get categorias => _categorias ??= $CategoriasTable(this);
  $EstadosTable _estados;
  $EstadosTable get estados => _estados ??= $EstadosTable(this);
  $MedidasTable _medidas;
  $MedidasTable get medidas => _medidas ??= $MedidasTable(this);
  $MedidaHasProdutosTable _medidaHasProdutos;
  $MedidaHasProdutosTable get medidaHasProdutos =>
      _medidaHasProdutos ??= $MedidaHasProdutosTable(this);
  $OrdensTable _ordens;
  $OrdensTable get ordens => _ordens ??= $OrdensTable(this);
  $PagamentosTable _pagamentos;
  $PagamentosTable get pagamentos => _pagamentos ??= $PagamentosTable(this);
  $ProdutosTable _produtos;
  $ProdutosTable get produtos => _produtos ??= $ProdutosTable(this);
  $SubCategoriasTable _subCategorias;
  $SubCategoriasTable get subCategorias =>
      _subCategorias ??= $SubCategoriasTable(this);
  $VendasTable _vendas;
  $VendasTable get vendas => _vendas ??= $VendasTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        categorias,
        estados,
        medidas,
        medidaHasProdutos,
        ordens,
        pagamentos,
        produtos,
        subCategorias,
        vendas
      ];
}
