part of 'Database.dart';
@DataClassName("Categoria")
class Categorias extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}