part of 'Database.dart';

@DataClassName("Produto")
class Produtos extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  TextColumn get descricao => text().withLength(max: 255)();
  TextColumn get codSistema => text().withLength(max: 50)();
  IntColumn get categoriaId => integer()
      .nullable()
      .customConstraint('NULL REFERENCES sub_categorias(id)')();
  IntColumn get quantidade => integer()();
  
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
