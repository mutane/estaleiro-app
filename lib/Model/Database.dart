import 'package:moor/moor.dart';
// These imports are only needed to open the database
import 'package:moor/ffi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'dart:io';
//part of generated tables of database
part 'Database.g.dart';
part 'Categoria.dart';
part 'Medida.dart';
part 'Ordem.dart';
part 'Pagamento.dart';
part 'Produto.dart';
part 'Estado.dart';
part 'SubCategoria.dart';
part 'Venda.dart';
part 'MedidaDoProduto.dart';

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(file);
  });
}

@UseMoor(tables: [
  Categorias,
  Estados,
  Medidas,
  MedidaHasProdutos,
  Ordens,
  Pagamentos,
  Produtos,
  SubCategorias,
  Vendas
])
class MyDatabase extends _$MyDatabase {
  // we tell the database where to store the data with this constructor
  MyDatabase() : super(_openConnection());
  // you should bump this number whenever you change or add a table definition. Migrations
  // are covered later in this readme.
  @override
  int get schemaVersion => 1;

  /// @categorias Crud

  //get all Categorias
  Future<List<Categoria>> get allCategoriasEntries => select(categorias).get();
  Stream<List<Categoria>> watchCategoriasEntries() {
    return (select(categorias)).watch();
  }

  Future<List<Categoria>> allCategoriasOrdered() {
    return (select(categorias)
          ..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Categorias as id

  Stream<Categoria> watchCategoriaById(int id) {
    return (select(categorias)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Categoria> categoriaById(int id) {
    return (select(categorias)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Categoria
  Future<int> addCategoria(Categoria entry) {
    return into(categorias).insert(entry);
  }

  Future<int> addCategoriaCompanion(CategoriasCompanion entry) {
    return into(categorias).insert(entry);
  }

  //update Categoria
  Future updateCategoria(Categoria entry) {
    return update(categorias).replace(entry);
  }

  Future updateCategoriaCompanion(CategoriasCompanion entry) {
    return update(categorias).replace(entry);
  }

  //delete all Categoria
  Future deleteAllCategorias() {
    // delete the oldest nine tasks
    return delete(categorias).go();
  }

  Future deleteCategoria(int entry) {
    // delete the oldest nine tasks
    return (delete(categorias)..where((t) => t.id.equals(entry))).go();
  }

  /// @Estados Crud

  //get all Estados
  Future<List<Estado>> get allEstadosEntries => select(estados).get();
  Stream<List<Estado>> watchEstadosEntries() {
    return (select(estados)).watch();
  }

  Future<List<Estado>> allEstadosOrdered() {
    return (select(estados)..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Estados as id

  Stream<Estado> watchEstadoById(int id) {
    return (select(estados)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Estado> estadoById(int id) {
    return (select(estados)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Estado
  Future<int> addEstado(Estado entry) {
    return into(estados).insert(entry);
  }

  Future<int> addEstadoCompanion(EstadosCompanion entry) {
    return into(estados).insert(entry);
  }

  //update Estado
  Future updateEstado(Estado entry) {
    return update(estados).replace(entry);
  }

  Future updateEstadoCompanion(EstadosCompanion entry) {
    return update(estados).replace(entry);
  }

  //delete all Estado
  Future deleteAllEstados() {
    // delete the oldest nine tasks
    return delete(estados).go();
  }

  Future deleteEstado(int entry) {
    // delete the oldest nine tasks
    return (delete(estados)..where((t) => t.id.equals(entry))).go();
  }

  /// @SubCategorias Crud

  //get all SubCategorias
  Future<List<SubCategoria>> get allSubCategoriasEntries =>
      select(subCategorias).get();
  Stream<List<SubCategoria>> watchSubCategoriasEntries() {
    return (select(subCategorias)).watch();
  }

  Future<List<SubCategoria>> allSubCategoriasOrdered() {
    return (select(subCategorias)
          ..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get SubCategorias as id

  Stream<SubCategoria> watchSubCategoriaById(int id) {
    return (select(subCategorias)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<SubCategoria> subCategoriaById(int id) {
    return (select(subCategorias)..where((t) => t.id.equals(id))).getSingle();
  }

  //add SubCategoria
  Future<int> addSubCategoria(SubCategoria entry) {
    return into(subCategorias).insert(entry);
  }

  Future<int> addSubCategoriaCompanion(SubCategoriasCompanion entry) {
    return into(subCategorias).insert(entry);
  }

  //update SubCategoria
  Future updateSubCategoria(SubCategoria entry) {
    return update(subCategorias).replace(entry);
  }

  Future updateSubCategoriaCompanion(SubCategoriasCompanion entry) {
    return update(subCategorias).replace(entry);
  }

  //delete all SubCategoria
  Future deleteAllSubCategorias() {
    // delete the oldest nine tasks
    return delete(subCategorias).go();
  }

  Future deleteSubCategoria(int entry) {
    // delete the oldest nine tasks
    return (delete(subCategorias)..where((t) => t.id.equals(entry))).go();
  }

  /// @Vendas Crud

  //get all Vendas
  Future<List<Venda>> get allVendasEntries => select(vendas).get();
  Stream<List<Venda>> watchVendasEntries() {
    return (select(vendas)).watch();
  }

  Future<List<Venda>> allVendasOrdered() {
    return (select(vendas)..orderBy([(t) => OrderingTerm(expression: t.id)]))
        .get();
  }
  //get Vendas as id

  Stream<Venda> watchVendaById(int id) {
    return (select(vendas)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Venda> vendaById(int id) {
    return (select(vendas)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Venda
  Future<int> addVenda(Venda entry) {
    return into(vendas).insert(entry);
  }

  Future<int> addVendaCompanion(VendasCompanion entry) {
    return into(vendas).insert(entry);
  }

  //update Venda
  Future updateVenda(Venda entry) {
    return update(vendas).replace(entry);
  }

  Future updateVendaCompanion(VendasCompanion entry) {
    return update(vendas).replace(entry);
  }

  //delete all Venda
  Future deleteAllVendas() {
    // delete the oldest nine tasks
    return delete(vendas).go();
  }

  Future deleteVenda(int entry) {
    // delete the oldest nine tasks
    return (delete(vendas)..where((t) => t.id.equals(entry))).go();
  }

  /// @Produtos Crud

  //get all Produtos
  Future<List<Produto>> get allProdutosEntries => select(produtos).get();
  Stream<List<Produto>> watchProdutosEntries() {
    return (select(produtos)).watch();
  }

  Future<List<Produto>> allProdutosOrdered() {
    return (select(produtos)
          ..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Produtos as id

  Stream<Produto> watchProdutoById(int id) {
    return (select(produtos)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Produto> produtoById(int id) {
    return (select(produtos)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Produto
  Future<int> addProduto(Produto entry) {
    return into(produtos).insert(entry);
  }

  Future<int> addProdutoCompanion(ProdutosCompanion entry) {
    return into(produtos).insert(entry);
  }

  //update Produto
  Future updateProduto(Produto entry) {
    return update(produtos).replace(entry);
  }

  Future updateProdutoCompanion(ProdutosCompanion entry) {
    return update(produtos).replace(entry);
  }

  //delete all Produto
  Future deleteAllProdutos() {
    // delete the oldest nine tasks
    return delete(produtos).go();
  }

  Future deleteProduto(int entry) {
    // delete the oldest nine tasks
    return (delete(produtos)..where((t) => t.id.equals(entry))).go();
  }

  /// @Pagamentos Crud

  //get all Pagamentos
  Future<List<Pagamento>> get allPagamentosEntries => select(pagamentos).get();
  Stream<List<Pagamento>> watchPagamentosEntries() {
    return (select(pagamentos)).watch();
  }

  Future<List<Pagamento>> allPagamentosOrdered() {
    return (select(pagamentos)
          ..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Pagamentos as id

  Stream<Pagamento> watchPagamentoById(int id) {
    return (select(pagamentos)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Pagamento> pagamentoById(int id) {
    return (select(pagamentos)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Pagamento
  Future<int> addPagamento(Pagamento entry) {
    return into(pagamentos).insert(entry);
  }

  Future<int> addPagamentoCompanion(PagamentosCompanion entry) {
    return into(pagamentos).insert(entry);
  }

  //update Pagamento
  Future updatePagamento(Pagamento entry) {
    return update(pagamentos).replace(entry);
  }

  Future updatePagamentoCompanion(PagamentosCompanion entry) {
    return update(pagamentos).replace(entry);
  }

  //delete all Pagamento
  Future deleteAllPagamentos() {
    // delete the oldest nine tasks
    return delete(pagamentos).go();
  }

  Future deletePagamento(int entry) {
    // delete the oldest nine tasks
    return (delete(pagamentos)..where((t) => t.id.equals(entry))).go();
  }

  /// @Ordens Crud

  //get all Ordens
  Future<List<Ordem>> get allOrdensEntries => select(ordens).get();
  Stream<List<Ordem>> watchOrdensEntries() {
    return (select(ordens)).watch();
  }

  Future<List<Ordem>> allOrdensOrdered() {
    return (select(ordens)..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Ordens as id

  Stream<Ordem> watchOrdemById(int id) {
    return (select(ordens)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Ordem> ordemById(int id) {
    return (select(ordens)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Ordem
  Future<int> addOrdem(Ordem entry) {
    return into(ordens).insert(entry);
  }

  Future<int> addOrdemCompanion(OrdensCompanion entry) {
    return into(ordens).insert(entry);
  }

  //update Ordem
  Future updateOrdem(Ordem entry) {
    return update(ordens).replace(entry);
  }

  Future updateOrdemCompanion(OrdensCompanion entry) {
    return update(ordens).replace(entry);
  }

  //delete all Ordem
  Future deleteAllOrdens() {
    // delete the oldest nine tasks
    return delete(ordens).go();
  }

  Future deleteOrdem(int entry) {
    // delete the oldest nine tasks
    return (delete(ordens)..where((t) => t.id.equals(entry))).go();
  }

  /// @MedidaHasProdutos Crud

  //get all MedidaHasProdutos
  Future<List<MedidaHasProduto>> get allMedidaHasProdutosEntries =>
      select(medidaHasProdutos).get();
  Stream<List<MedidaHasProduto>> watchMedidaHasProdutosEntries() {
    return (select(medidaHasProdutos)).watch();
  }

  Future<List<MedidaHasProduto>> allMedidaHasProdutosOrdered() {
    return (select(medidaHasProdutos)
          ..orderBy([(t) => OrderingTerm(expression: t.id)]))
        .get();
  }
  //get MedidaHasProdutos as id

  Stream<MedidaHasProduto> watchMedidaHasProdutoById(int id) {
    return (select(medidaHasProdutos)..where((t) => t.id.equals(id)))
        .watchSingle();
  }

  Future<MedidaHasProduto> medidaHasProdutoById(int id) {
    return (select(medidaHasProdutos)..where((t) => t.id.equals(id)))
        .getSingle();
  }

  //add MedidaHasProduto
  Future<int> addMedidaHasProduto(MedidaHasProduto entry) {
    return into(medidaHasProdutos).insert(entry);
  }

  Future<int> addMedidaHasProdutoCompanion(MedidaHasProdutosCompanion entry) {
    return into(medidaHasProdutos).insert(entry);
  }

  //update MedidaHasProduto
  Future updateMedidaHasProduto(MedidaHasProduto entry) {
    return update(medidaHasProdutos).replace(entry);
  }

  Future updateMedidaHasProdutoCompanion(MedidaHasProdutosCompanion entry) {
    return update(medidaHasProdutos).replace(entry);
  }

  //delete all MedidaHasProduto
  Future deleteAllMedidaHasProdutos() {
    // delete the oldest nine tasks
    return delete(medidaHasProdutos).go();
  }

  Future deleteMedidaHasProduto(int entry) {
    // delete the oldest nine tasks
    return (delete(medidaHasProdutos)..where((t) => t.id.equals(entry))).go();
  }

  /// @Medidas Crud

  //get all Medidas
  Future<List<Medida>> get allMedidasEntries => select(medidas).get();
  Stream<List<Medida>> watchMedidasEntries() {
    return (select(medidas)).watch();
  }

  Future<List<Medida>> allMedidasOrdered() {
    return (select(medidas)..orderBy([(t) => OrderingTerm(expression: t.nome)]))
        .get();
  }
  //get Medidas as id

  Stream<Medida> watchMedidaById(int id) {
    return (select(medidas)..where((t) => t.id.equals(id))).watchSingle();
  }

  Future<Medida> medidaById(int id) {
    return (select(medidas)..where((t) => t.id.equals(id))).getSingle();
  }

  //add Medida
  Future<int> addMedida(Medida entry) {
    return into(medidas).insert(entry);
  }

  Future<int> addMedidaCompanion(MedidasCompanion entry) {
    return into(medidas).insert(entry);
  }

  //update Medida
  Future updateMedida(Medida entry) {
    return update(medidas).replace(entry);
  }

  Future updateMedidaCompanion(MedidasCompanion entry) {
    return update(medidas).replace(entry);
  }

  //delete all Medida
  Future deleteAllMedidas() {
    // delete the oldest nine tasks
    return delete(medidas).go();
  }

  Future deleteMedida(int entry) {
    // delete the oldest nine tasks
    return (delete(medidas)..where((t) => t.id.equals(entry))).go();
  }
}
