part of 'Database.dart';

@DataClassName("SubCategoria")
class SubCategorias extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  IntColumn get categoriaId =>
      integer().nullable().customConstraint('NULL REFERENCES categorias(id)')();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
