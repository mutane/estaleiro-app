part of 'Database.dart';

@DataClassName("MedidaHasProduto")
class MedidaHasProdutos extends Table {
  IntColumn get id => integer().autoIncrement()();
  RealColumn get preco => real()();
  IntColumn get produtoId =>
      integer().nullable().customConstraint('NULL REFERENCES produtos(id)')();
  IntColumn get medidaId =>
      integer().nullable().customConstraint('NULL REFERENCES medidas(id)')();
  RealColumn get desconto => real()();
  IntColumn get numeroDeProdutosPorMedida =>
      integer().withDefault(const Constant(1))();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
