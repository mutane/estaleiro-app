part of 'main.dart';
void databaseTest() async {
  final database = MyDatabase();
  database.deleteAllCategorias();
  database.deleteAllSubCategorias();
  database.deleteAllVendas();
  database.deleteAllEstados();
  database.deleteAllProdutos();
  database.deleteAllMedidaHasProdutos();
  database.deleteAllOrdens();
  database.deleteAllMedidas();
  database.deleteAllPagamentos();
  var items = [1];
  for (var item in items) {
    item = item++;
    database.addCategoria(new Categoria(
        nome: "Ferro",
        createdAt: DateTime.now(),
        id: null,
        updatedAt: DateTime.now()));
    database.addSubCategoria(new SubCategoria(
        nome: "Varao 8",
        createdAt: DateTime.now(),
        id: null,
        categoriaId: 1,
        updatedAt: DateTime.now()));
    database.addPagamento(new Pagamento(
        nome: "M-Pesa",
        createdAt: DateTime.now(),
        id: null,
        updatedAt: DateTime.now()));
    database.addMedida(new Medida(
        id: null,
        nome: "Kilos",
        createdAt: DateTime.now(),
        updatedAt: DateTime.now()));
    database.addEstado(new Estado(
        id: null,
        nome: "Pago",
        createdAt: DateTime.now(),
        updatedAt: DateTime.now()));
    final uuid = Uuid().v4();
    database.addOrdem(new Ordem(
        id: null,
        nome: uuid,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now()));
    database.addProduto(new Produto(
        id: null,
        nome: "Mulher",
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        codSistema: 'Sxpjkvm',
        descricao: 'Produto mais doce da face da terra',
        quantidade: 30));

    database.addMedidaHasProduto(new MedidaHasProduto(
        id: null,
        produtoId: 30,
        medidaId: 2,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        desconto: 30,
        numeroDeProdutosPorMedida: 3,
        preco: 37.5));

    database.addVenda(new Venda(
        id: null,
        produto: 1,
        pagamentoId: 1,
        quantidade: 56,
        ordem: 1,
        status: 1,
        foiDescontado: true,
        vendidoA: 980,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now()));
  }
  final cats = await database.allCategoriasEntries;
  cats.forEach((e) => print(e));
  final est = await database.allEstadosEntries;
  est.forEach((e) => print(e));
  final mdd = await database.allMedidasEntries;
  mdd.forEach((e) => print(e));
  final mddP = await database.allMedidaHasProdutosEntries;
  mddP.forEach((e) => print(e));
  final ord = await database.allOrdensEntries;
  ord.forEach((e) => print(e));
  final pgm = await database.allPagamentosEntries;
  pgm.forEach((e) => print(e));
  final prd = await database.allProdutosEntries;
  prd.forEach((e) => print(e));
  final subcats = await database.allSubCategoriasEntries;
  subcats.forEach((e) => print(e));

  final venda = await database.allVendasEntries;
  venda.forEach((e) => print(e));
  final squemas = database.allSchemaEntities;
  squemas.forEach((e) => print(e.entityName));
}
